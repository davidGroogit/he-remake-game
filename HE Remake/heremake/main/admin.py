from django.contrib import admin

# Register your models here.
from .models import *

admin.site.side_header = "SkyHacks"
admin.site.site_title = "SkyHacks Admin Area"
admin.site.index_title = "Welcome to the SkyHacks Admin Area"


'''
Admin display for all User related informations
'''
class HEUserSoftwareInformationAdmin(admin.StackedInline):
    model = HEUserSoftwareInformation
    extra = 0
class HEUserInformationAdmin(admin.StackedInline):
    model = HEUserInformation
    extra = 0
class HEUserHardwareInformationAdmin(admin.TabularInline):
    model = HEUserHardwareInformation
    extra= 0
class HEUserAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldsets = [
    ('Date Registered', {'fields': ['date_joined', 'id']}),
    ('Login Information', {'fields': ['username', 'email', 'password'], 'classes': ['collapse']}),
    ('User Account Rep and Balances', {'fields': ['reputation', 'total_cash_balance', 'bitcoin_balance'], 'classes': ['collapse']}),
    ]
    inlines = [HEUserInformationAdmin, HEUserHardwareInformationAdmin, HEUserSoftwareInformationAdmin]

'''
Admin display for all Clan related informations
'''
class ClanHardwareInformationAdmin(admin.TabularInline):
    model = ClanHardwareInformation
    extra = 0
class ClanInformationAdmin(admin.StackedInline):
    model = ClanInformation
    extra = 0
class ClanAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldsets = [
        ('Public Information', {'fields': ['clan_name', 'id', 'clan_power', 'clan_population', 'clan_rank']}),
    ]
    inlines = [ClanHardwareInformationAdmin, ClanInformationAdmin]

'''
Admin display for all processor related informations
'''
class ProcessorInformationAdmin(admin.StackedInline):
    model = ProcessorInformation
    extra = 0
class ProcessorAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldsets = [
        ('Processors', {'fields': ['processor_name', 'id']}),
    ]
    inlines = [ProcessorInformationAdmin]

'''
Admin display for all graphics card (GPU) related informations
'''
class GraphicsCardInformationAdmin(admin.StackedInline):
    model = GraphicsCardInformation
    extra = 0

class GraphicsCardAdmin(admin.ModelAdmin):
    readonly_fields = ('id')
    fieldsets = [
        ('Processors', {'fields': ['gc_name', 'id']}),
    ]
    inlines = [GraphicsCardInformationAdmin]


'''
Admin display for all hard drive related information
'''

class HDDInformationAdmin(admin.StackedInline):
    model = HDDInformation
    extra = 0
class HDDAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldsets = [
        ("Hard Drives", {'fields': ['hdd_name', 'id']})
    ]
    inlines = [HDDInformationAdmin]

'''
Admin display for all RAM related informations
'''
class RAMInformationAdmin(admin.StackedInline):
    model = RAMInformation
    extra = 0
class RAMAdmin(admin.ModelAdmin):
    fieldsets = [
        ("RAM Inventory", {'fields': ['ram_name']})
    ]
    inlines = [RAMInformationAdmin]

'''
Admin display for all Network related informations
'''
class NetworkInformationAdmin(admin.StackedInline):
    model = NetworkInformation
    extra = 0
class NetworkAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldsets = [
        ("Network Inventory", {"fields": ['network_name', 'id']})
    ]
    inlines = [NetworkInformationAdmin]

'''
Admin display for all Software related informations
'''
class SoftwareInformationAdmin(admin.StackedInline):
    model = SoftwareInformation
    extra = 0
class SoftwareTypeAdmin(admin.TabularInline):
    model = SoftwareType
    extra = 0
class SoftwareLicenseInformationAdmin(admin.StackedInline):
    model = SoftwareLicenseInformation
    extra = 0
class SoftwareAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldsets = [
        ("Software Inventory", {"fields": ['software_name', 'id']})
    ]
    inlines = [SoftwareTypeAdmin, SoftwareLicenseInformationAdmin, SoftwareInformationAdmin]

'''
Admin display for all Bank related informations
'''
class BankInformationAdmin(admin.StackedInline):
    model = BankInformation
    extra = 0
class BankHardwareInformationAdmin(admin.TabularInline):
    model = BankHardwareInformation
    extra = 0
class BankSoftwareInformationAdmin(admin.StackedInline):
    model = BankSoftwareInformation
    extra = 0
class BankAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldset = [
        ("Bank Inventory", {'fields': ['bank_name', 'id']})
    ]
    inlines = [BankInformationAdmin, BankHardwareInformationAdmin, BankSoftwareInformationAdmin]

'''
Admin display for all Company related informations
'''
class CompanyInformationAdmin(admin.StackedInline):
    model = CompanyInformation
    extra = 0
class CompanyHardwareInformationAdmin(admin.TabularInline):
    model = CompanyHardwareInformation
    extra = 0
class CompanySoftwareInformationAdmin(admin.StackedInline):
    model = CompanySoftwareInformation
    extra = 0
class CompanyAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldset = [
        ("Company Inventory", {'fields': ['company_name', 'id']})
    ]
    inlines = [CompanyInformationAdmin, CompanyHardwareInformationAdmin, CompanySoftwareInformationAdmin]


class IPAddressAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    fieldset = [
        ("IP Inventory", {'fields': ['ip', 'id']})
    ]

admin.site.register(IPAddress, IPAddressAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Bank, BankAdmin)
admin.site.register(Software, SoftwareAdmin)
admin.site.register(Network, NetworkAdmin)
admin.site.register(HEUser, HEUserAdmin)
admin.site.register(Processor, ProcessorAdmin)
admin.site.register(GraphicsCard, GraphicsCardAdmin)
admin.site.register(RAM, RAMAdmin)
admin.site.register(HDD, HDDAdmin)
admin.site.register(Clan, ClanAdmin)

#admin.site.register(HEUserHardwareInformation)