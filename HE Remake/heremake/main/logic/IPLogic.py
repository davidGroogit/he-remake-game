from ..models import IPAddress
from random import randint, choice
from string import hexdigits

'''==============================================='''
'''                                               '''
'''               FETCHING LOGIC                  '''
'''                                               '''
'''==============================================='''


'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching the ip addresses
'''''''''''''''''''''''''''''''''''''''''''''''''''''
def getIps():
    return IPAddress.objects.all()

def getIpByIp(ip):
    return IPAddress.objects.get(ip=ip)

def getIpById(id):
    return IPAddress.objects.get(id=id)

'''==============================================='''
'''                                               '''
'''               CREATING LOGIC                  '''
'''                                               '''
'''==============================================='''

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to creating the ip addresses
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Generate an ip based on the ip version (ipv4, ipv6)
def generateIpAddress(version):
    if version == 4:
        octets = []
        for i in range(4):
            octets.append(str(randint(1,254)))
        return '.'.join(octets)
    elif version == 6:
        octets = []
        for i in range(8):
            octet = []
            for i in range(4):
                octet.append(str(choice(hexdigits.lower())))
            octets.append(''.join(octet))
        return ':'.join(octets)
    else:
        return


#Add ip to database if it passes the validation
def addIp(id, version, ownertype):
    #do while loop implementation
    ip = generateIpAddress(version)
    exists = validateIpAddress(ip)

    while(exists):
        ip = generateIpAddress(version)
        exists = validateIpAddress(ip)

    generatedIp = IPAddress.objects.create()
    generatedIp.ip = ip
    generatedIp.ip_version = version
    generateIpId = generatedIp.id
    generatedIp.ip_owner_id = id
    generatedIp.ip_ownertype = ownertype

    generatedIp.save()

    return getIpById(generateIpId)

#Validate ip to make sure it does not already exist in database
def validateIpAddress(ip): 
    all_ips = getIps()

    for existingip in all_ips:
        if existingip.ip == ip:
            return True
        else:
            return False



'''============================================='''
'''                                             '''
'''               EDITING LOGIC                 '''
'''                                             '''
'''============================================='''

'''''''''''''''''''''''''''''''''''''''''''''''''''
All services/logic linked to editing IP addresses
'''''''''''''''''''''''''''''''''''''''''''''''''''
def editIpAddress(oldip, newip, randomized, userid, version, ownertype):
    ip = getIpByIp(oldip)
    
    # If randomized, generate ip until valid ip is found
    if randomized == 1:
        tempip = generateIpAddress(version)
        exists = validateIpAddress(tempip)
        while exists:
            tempip = generateIpAddress(version)
            exists = validateIpAddress(tempip)
        ip.ip = tempip
    # Else validate the manually added address
    else:
        exists = validateIpAddress(newip)
        if exists:
            return False
        else:
            ip.ip = newip
   
    ip.ip_version = version
    ip.ip_owner_id = userid #MAY CHANGE TO USE OLD IP OWNER ID DEPENDING ON FUTURE NEEDS
    ip.ip_ownertype = ownertype

    ip.save()





