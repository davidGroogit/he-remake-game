from ..models import HEUser, HEUserInformation, HEUserHardwareInformation
from ..logic.IPLogic import addIp, getIpByIp

'''==============================================='''
'''                                               '''
'''               FETCHING LOGIC                  '''
'''                                               '''
'''==============================================='''

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching the users directly
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all users following a limit and filters set.
def getUsers():
    return HEUser.objects.all()

#Get user by id
def getUserById(userid):
    return HEUser.objects.get(id=userid)

#Get User by username
def getUserByUsername(username):
    response = HEUser.objects.get(username=username)
    return response

#Get user by ip address
def getUserByIp(ip):
    return HEUser.objects.get(ip_address=ip)

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching the user information
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all user informations
def getUserInformations():
    return HEUserInformation.objects.all()

#Get user information via userid
def getUserInformationById(userid):
    return HEUserInformation.objects.get(user=getUserById(userid)) 

#Get userinformation via username
def getUserInformationByUsername(username):
    return HEUserInformation.objects.get(user=getUserByUsername(username)) 

#Get user informtion via ip address
def getUserInformationByIp(ip):
    return HEUserInformation.objects.get(ip_address=ip)

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching the hardware
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all user hardwares available
def getUserHardware():
    hardware = HEUserHardwareInformation.objects.all()
    return hardware

#Get a certain hardware via ip address
def getUserHardwareByIp(ip):
    user = getUserByIp(ip)
    hardware = HEUserHardwareInformation.objects.get(user=user)
    return hardware

def getUserHardwareByUsername(username):
    user = getUserByUsername(username)
    hardware = HEUserHardwareInformation.objects.get(user=user)
    return hardware


'''==============================================='''
'''                                               '''
'''               CREATING LOGIC                  '''
'''                                               '''
'''==============================================='''

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to adding the users directly
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Add user
def createUser(username, password, email):
    user = HEUser.objects.create()
    user.username = username
    user.email = email
    user.password = password
    user.save()

    generatedIp = addIp(user.id, 6)

    #Generate user information and save to db
    userinfo = HEUserInformation(
        ip_address=generatedIp,
        clan_id=0,
        user=user
    )

    userinfo.save()

    #Generate user hardware and save to db
    userhardware = HEUserHardwareInformation(
        processor_id=1,
        hdd_id=1,
        ram_id=1,
        network_speed_id=1,
        graphics_card_id=1,
        user=user,
    )
    userhardware.save()


'''==============================================='''
'''                                               '''
'''                EDITING LOGIC                  '''
'''                                               '''
'''==============================================='''

#Edit user total cash balance based the userid, operation argument and the amount
def editUserTotalCashBalance(userid, operation, amount):
    user = getUserById(userid)
    if operation == 'increment':
        user.total_cash_balance += amount
    elif operation == 'decrement':
        user.total_cash_balance -= amount
    else:
        user.total_cash_balance = user.total_cash_balance
    user.save()

#Edit user bitcoin balance based the userid, operation argument and the amount
def editUserBitcoinBalance(userid, operation, amount):
    user = getUserById(userid)
    if operation == 'increment':
        user.bitcoin_balance += amount
    elif operation == 'decrement':
        user.bitcoin_balance -= amount
    else:
        user.bitcoin_balance = user.bitcoin_balance
    user.save()

def editUserReputation(userid, operation, amount):
    user = getUserById(userid)
    if operation == 'increment':
        user.reputation += int(amount)
    elif operation == 'decrement':
        user.reputation -= int(amount)
    else:
        user.reputation = user.reputation
    user.save()