from ..models import Processor, ProcessorInformation, HDD, HDDInformation, RAM, RAMInformation, Network, NetworkInformation


'''==============================================='''
'''                                               '''
'''               FETCHING LOGIC                  '''
'''                                               '''
'''==============================================='''

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching processors
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all processors
def getProcessors():
    return Processor.objects.all()
#Get processor via ID
def getProcessorById(procid):
    return Processor.objects.get(id=procid)
'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching processor information
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all processor informations
def getProcessorInfos():
    return ProcessorInformation.objects.all()
#Get Processor Information via processor ID
def getProcessorInfoByProcessorId(procid):
    return ProcessorInformation.objects.get(processor=getProcessorById(procid))
#Get processor via Block Speed
def getProcessorInfoBySpeed(clockSpeed, sortby="id", order="desc"):
    if order == "asc":
        return ProcessorInformation.objects.filter(processor_speed=clockSpeed).order_by(sortby)
    else:
        return ProcessorInformation.objects.filter(processor_speed=clockSpeed).order_by(sortby).reverse()
#Get Processor via Price
def getProcessorInfoByPrice(price, sortby="price", order="desc"):
    if order == "asc":
        return ProcessorInformation.objects.filter(processor_price=price).order_by(sortby)
    else:
        return ProcessorInformation.objects.filter(processor_price=price).order_by(sortby).reverse()
#Get Processor via Model Number
def getProcessorInfoByModel(model):
    return ProcessorInformation.objects.get(processor_model_number=model)
#Get processor by core amount
def getProcessorInfoByCoreAmount(cores, sortby="price", order="desc"):
    if order == "asc":
        return ProcessorInformation.objects.filter(processor_cores=cores).order_by(sortby)
    else:
        return ProcessorInformation.objects.filter(processor_cores=cores).order_by(sortby).reverse()

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching hard drives
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all hard drives
def getHDDs():
    return HDD.objects.all()
#Get hard drive via ID
def getHDDById(hddid):
    return HDD.objects.get(id=hddid)

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching hard drive information
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all hard drive informations
def getHDDInfos(sortby="id", order="desc"):
    if order == "asc":
        return HDDInformation.objects.all().order_by(sortby)
    else:
        return HDDInformation.objects.all().order_by(sortby).reverse()
#Get HDD info via HDD ID
def getHDDInfoByHDDId(hddid):
    return HDDInformation.objects.get(id=getHDDById(hddid))
#Get HDDs by size
def getHDDBySize(size, sortby="id", order="desc"):
    if order == "asc":
        return HDDInformation.objects.filter(hdd_size=size).order_by(sortby)
    else:
        return HDDInformation.objects.filter(hdd_size=size).order_by(sortby).reverse()
#Get HDD by Read Speed
def getHDDByReadSpeed(speed, sortby="id", order="desc"):
    if order == "asc":
        return HDDInformation.objects.filter(hdd_read_speed=speed).order_by(sortby)
    else:
        return HDDInformation.objects.filter(hdd_read_speed=speed).order_by(sortby).reverse()
#Get HDD by Write Speed
def getHDDByWriteSpeed(speed, sortby="id", order="desc"):
    if order == "asc":
        return HDDInformation.objects.filter(hdd_write_speed=speed).order_by(sortby)
    else:
        return HDDInformation.objects.filter(hdd_write_speed=speed).order_by(sortby).reverse()
#Get HDD via Model number
def getHDDByModelNumber(model):
    return HDDInformation.objects.get(hdd_model=model)
#Get HDD via Price
def getHDDByPrice(price, sortby="id", order="desc"):
    if order == "asc":
        return HDDInformation.objects.filter(hdd_price=price).order_by(sortby)
    else:
        return HDDInformation.objects.filter(hdd_price=price).order_by(sortby).reverse()
#Get hard drives via type
def getHDDByType(type, sortby="id", order="desc"):
    if order == "asc":
        return HDDInformation.objects.filter(hdd_type=type).order_by(sortby)
    else:
        return HDDInformation.objects.filter(hdd_type=type).order_by(sortby).reverse()

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching RAM
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all RAM
def getRAMs():
    return RAM.objects.all()
#Get RAM via ID
def getRAMById(ramid):
    return RAM.objects.get(id=ramid)

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching RAM information
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all RAM infos
def getRAMInfos():
    return RAMInformation.objects.all()
#Get RAM information via RAM id
def getRAMInfosById(ramid):
    return RAMInformation.objects.get(ram_id=ramid)
#Get RAM via model number
def getRAMInfosByModel(model):
    return RAMInformation.objects.get(ram_model=model)
#Get RAM information via speed
def getRAMInfosBySpeed(speed, sortby="id", order="desc"):
    if order == "asc":
        return RAMInformation.objects.filter(ram_speed=speed).order_by(sortby).reverse()
    else:
        return RAMInformation.objects.filter(ram_speed=speed).order_by(sortby)
#Get RAM via price
def getRAMInfosByPrice(price, sortby="id", order="desc"):
    if order == "asc":
        return RAMInformation.objects.filter(ram_price=price).order_by(sortby).reverse()
    else:
        return RAMInformation.objects.filter(ram_price=price).order_by(sortby)   

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching Networks
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all networks
def getNetworks():
    return Network.objects.all()
#Get network via ID
def getNetworkById(netid):
    return Network.objects.get(id=netid)

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to fetching Network information
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Get all network informations
def getNetworkInfos():
    return NetworkInformation.objects.all()
#Get Network info via network ID
def getNetworkInfoById(netid):
    return Network.objects.get(network_id=netid)

'''==============================================='''
'''                                               '''
'''               CREATING LOGIC                  '''
'''                                               '''
'''==============================================='''

'''''''''''''''''''''''''''''''''''''''''''''''''''''
All services linked to adding the hardware directly
'''''''''''''''''''''''''''''''''''''''''''''''''''''
#Add processor
def createProcessor(name, cores, model, price, speed):
    proc = Processor.objects.create()
    proc.processor_name = name
    proc.save()

    procInfo = ProcessorInformation.objects.create(
        processor = proc,
        processor_cores = cores,
        processor_model_number = model,
        processor_price = price,
        processor_speed = speed
    )
    
    procInfo.save()



#Add Hard Disk Drive
def createHDD(name, hddtype, model, rspeed, wspeed, price, size):
    hdd = HDD.objects.create()
    hdd.hdd_name = name
    hdd.save()

    hddInfo = HDDInformation.objects.create(
        hdd = hdd,
        hdd_type = hddtype,
        hdd_model = model,
        hdd_size = size,
        hdd_read_speed = rspeed,
        hdd_write_speed = wspeed,
        hdd_price = price
    )

    hddInfo.save()

