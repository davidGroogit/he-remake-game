from django.db import models
from django.contrib.auth.models import AbstractUser

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to the ip addresses information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Base class for IP Addresses
class IPAddress(models.Model):
    ip = models.CharField(max_length=39, unique=True)
    ip_version = models.IntegerField(default=4)
    ip_owner_id = models.IntegerField(default=0)
    ip_ownertype = models.IntegerField(default=0)
    def __str__(self):
        return str(self.ip)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to the ip addresses information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Base class for all software
class Software(models.Model):
    software_name = models.CharField(max_length=50)
    def __str__(self):
        return str(self.software_name)

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to the User login and game information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

#Main User model, which extends Abstract User
class HEUser(AbstractUser):
    reputation = models.IntegerField(default=0)
    bitcoin_balance = models.DecimalField(default=0, decimal_places=2, max_digits=60)
    total_cash_balance = models.DecimalField(default=0, decimal_places=2, max_digits=60)
    def __str__(self):
        return str(self.username)
    @property
    def slug(self):
        return self.username
#Model for the general user information in game
class HEUserInformation(models.Model):
    user = models.ForeignKey(HEUser, on_delete=models.CASCADE)
    ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    clan_id = models.IntegerField(default=0)

    def __str__(self):
        return str(self.user)
#Model for the user's hardware information
class HEUserHardwareInformation(models.Model):
    user = models.ForeignKey(HEUser, on_delete=models.CASCADE)
    processor_id = models.IntegerField(default=0)
    hdd_id = models.IntegerField(default=0)
    ram_id = models.IntegerField(default=0)
    network_speed_id = models.IntegerField(default=0)
    graphics_card_id = models.IntegerField(default=0)

    def __str__(self):
        return str(self.user)

class HEUserSoftwareInformation(models.Model):
    user = models.ForeignKey(HEUser, on_delete=models.CASCADE)
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.user)
        
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to Clans information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Main clan model
class Clan(models.Model):
    clan_name = models.CharField(max_length=50)
    clan_power = models.IntegerField(default=0)
    clan_population = models.IntegerField(default=1)
    clan_rank = models.IntegerField(default=99999)

    def __str__(self):
        return str(self.clan_name)
#General information model
class ClanInformation(models.Model):
    clan = models.ForeignKey(Clan, on_delete=models.CASCADE)
    clan_ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.clan)
#Hardware information model
class ClanHardwareInformation(models.Model):
    clan = models.ForeignKey(Clan, on_delete=models.CASCADE)
    processor_id = models.IntegerField(default=0)
    hdd_id = models.IntegerField(default=0)
    ram_id = models.IntegerField(default=0)
    network_speed_id = models.IntegerField(default=0)
    graphics_card_id = models.IntegerField(default=0)
    def __str__(self):
        return str(self.clan)

class ClanSoftwareInformation(models.Model):
    clan = models.ForeignKey(Clan, on_delete=models.CASCADE)
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.clan)

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to Software information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
class SoftwareType(models.Model):
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    software_type_name = models.CharField(max_length=25)
    def __str__(self):
        return str(self.software_type_name)

class SoftwareInformation(models.Model):
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    software_version = models.FloatField(default=1.0)
    software_size = models.FloatField(default=1.0)
    software_ram_usage = models.FloatField(default=1.0)
    software_rank = models.CharField(default='Useless', max_length=25)
    def __str__(self):
        return str(self.software)

class SoftwareLicenseInformation(models.Model):
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    software_license_owner = models.ForeignKey(HEUser, on_delete=models.CASCADE)
    software_license_price = models.FloatField(default=10.0)
    def __str__(self):
        return str(self.software)

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to the processor hardware information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Main processor model
class Processor(models.Model):
    processor_name = models.CharField(max_length=50)

    def __str__(self):
        return self.processor_name
#Single processor information model
class ProcessorInformation(models.Model):
    processor = models.ForeignKey(Processor, on_delete=models.CASCADE)
    processor_model_number = models.CharField(default='S0-0000', max_length=7)
    processor_cores = models.IntegerField(default=1)
    processor_speed = models.FloatField(default=1.0)
    processor_price = models.FloatField(default=1.0)

    def __str__(self):
        return str(self.processor)

#Primary Graphics Card model
class GraphicsCard(models.model):
    gc_name = mdoels.CharField(max_length=50)

    def __str__(self):
        return self.gc_name
#Graphics card inforamtion model
class GraphicsCardInformation(models.Model):
    gc = models.ForeignKey(GraphicsCard, on_delete=models.CASCADE)
    gc_model_number = models.CharField(default='GPU', max_length=10)
    gc_cores = models.IntegerField(default=0)
    gc_rays = models.IntegerField(default=0)            #Only use rays if the card supports raytracing, else it stays at 0
    gc_base_clock = models.IntegerField(default=0)      #MHz
    gc_boost_clock = models.IntegerField(default=0)     #MHz
    gc_mem_speed = models.IntegerField(default=0)       #Gbps
    gc_mem_config = models.IntegerField(default=0)      #GB DDR6 Video RAM
    gc_bandwith = models.IntegerField(default=0)        #GB/sec
    gc_rtx_support = models.IntegerField(default=0)     # 0 = No Support, 1 = Raytracing support
    gc_max_temp = models.IntegerField(default=40)       # Celsius
    gc_power_usage = models.IntegerField(default=100)   # Watts

    def __str__(self):
        return str(self.gc)

#Main hard drive mdoel
class HDD(models.Model):
    hdd_name = models.CharField(max_length=50)

    def __str__(self):
        return self.hdd_name
#Model for general hard drive information
class HDDInformation(models.Model):
    hdd = models.ForeignKey(HDD, on_delete=models.CASCADE)
    hdd_type = models.CharField(max_length=20)
    '''
    How model number is chosen:
        2 first chars are for brand
        2-4 next chars are for capacity
        2 next chars are for segment
        Last char is for generation
    '''
    hdd_model = models.CharField(max_length=20, default='XX00XXA')
    hdd_read_speed = models.IntegerField(default=0)
    hdd_write_speed = models.IntegerField(default=0)
    hdd_price = models.FloatField(default=100.0)
    hdd_size = models.FloatField(default=100.0)
    def __str__(self):
        return str(self.hdd)

#Main RAM model
class RAM(models.Model):
    ram_name = models.CharField(max_length=30)
    def __str__(self):
        return self.ram_name
#Model for the specific ram general information
class RAMInformation(models.Model):
    ram = models.ForeignKey(RAM, on_delete=models.CASCADE)
    '''
    How model number is chosen:
        2 first chars are for brand
        1 char is for type (DDR4, DDR3)
        2 chars for speed (26=2666)
        4 chars models name
        4 chars for amount (2/16 = 2 sticks of 8 to give 16)
    '''
    ram_model = models.CharField(max_length=30, default='XX426ABCD2/16')
    ram_speed = models.IntegerField(default=1000)
    ram_price = models.FloatField(default=100.0)
    def __str__(self):
        return str(self.ram)

#Main Network model
class Network(models.Model):
    network_name = models.CharField(max_length=50)
    def __str__(self):
        return str(self.network_name)   
#Model for the general information of a specific network
class NetworkInformation(models.Model):
    network = models.ForeignKey(Network, on_delete=models.CASCADE)
    network_speed = models.FloatField(default=10.0)
    network_price = models.FloatField(default=100.0)

    def __str__(self):
        return str(self.network)


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to  Banks information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Main Bank model
class Bank(models.Model):
    bank_name = models.CharField(max_length=50)
    def __str__(self):
        return str(self.bank_name)

class BankInformation(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    bank_ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.bank)

class BankHardwareInformation(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    processor_id = models.IntegerField(default=0)
    hdd_id = models.IntegerField(default=0)
    ram_id = models.IntegerField(default=0)
    network_speed_id = models.IntegerField(default=0)
    graphics_card_id = models.IntegerField(default=0)
    def __str__(self):
        return str(self.bank)

class BankSoftwareInformation(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    software = models.ForeignKey(Software, on_delete=models.CASCADE, default=1)
    def __str__(self):
        return str(self.software)


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
All models related to the company information
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Main model for company
class Company(models.Model):
    company_name = models.CharField(max_length=50)
    def __str__(self):
        return str(self.company_name)

class CompanyInformation(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    company_ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.company)

class CompanyHardwareInformation(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    processor_id = models.IntegerField(default=0)
    hdd_id = models.IntegerField(default=0)
    ram_id = models.IntegerField(default=0)
    network_speed_id = models.IntegerField(default=0)
    graphics_card_id = models.IntegerField(default=0)
    def __str__(self):
        return str(self.company)

class CompanySoftwareInformation(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.company)


