from django.shortcuts import render
from .models import HEUser
from .logic.UserLogic import *
from .logic.HardwareLogic import *

from datetime import datetime


def generateInitialData():
    #Initial data insertion
    #createUser(username='NuclearUser', email='nucleartest@email.com', password='nuclearpass')
    #createUser(username='JakeBander', email='jake@email.com', password='bondpass')
    #createProcessor("AMP Frozen 3900X", 6, "FR3900X", 345.67, 2666)
    #createHDD("Eastern Digital RED [1tb]", 'mechanical', "ED1000DKA", 6000, 6000,89.99, 1000)
    editUserTotalCashBalance(1,'increment', round(1657.94,2))
    editUserBitcoinBalance(1,'increment', round(0.058,2))
    editUserReputation(1, 'increment', 1.2)
    pass


def index(request):
    hardrives = getHDDInfos('hdd_price', 'asc')
    hardrivesizes = getHDDBySize(500, 'hdd_size', 'asc')
    processors = getProcessorInfos()
    rams = getRAMInfos()
    
    

    now = datetime.now()
    datetime_now = now.strftime("%d/%m/%Y %H:%M:%S")
    generateInitialData()
    all_users = getUserInformations()
    context = {
        'all_users': all_users, 
        'hardrives': hardrives, 
        'hardrivesizes': hardrivesizes,
        'processors': processors,
        'rams': rams,
        'datetime_now': datetime_now,
        }


    return render(request, 'main/index.html', context)
